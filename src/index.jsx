import React from 'react';
import {render} from 'react-dom';
import {HashRouter} from 'react-router-dom';
import {FeedbackWedget} from './feedback-wedget.component';
import {Provider} from 'react-redux';
import qs from 'query-string';
import store from './store';

(() => {
  const query = qs.parse(location.search);
  if (typeof query.widget_url === 'undefined') {
    return alert('?widget_urlを指定してください');
  }

  const root = document.createElement('div');
  document.body.appendChild(root);

  render(
    <Provider store={store}>
      <HashRouter>
        <FeedbackWedget widgetUrl={query.widget_url} referer={query.referer} />
      </HashRouter>
    </Provider>,
    root
  );
})();
