import qs from 'query-string';

const createIframe = src => {
  const iframe = document.createElement('iframe');
  iframe.style.cssText = 'width:100%;border:none';
  iframe.src = src;

  return iframe;
}

const activateWidget = (query, id) => {
  if (typeof query === 'undefined' || typeof query.widget_url === 'undefined') {
    throw new Error('query.widget_urlを指定してください');
  }

  if (typeof query === 'undefined' || typeof query.referer === 'undefined') {
    throw new Error('query.refererを指定してください');
  }

  if (typeof id === 'undefined') {
    throw new Error('script[id]を指定してくください');
  }

  const script = document.getElementById(id);
  const iframe = createIframe(`https://widget.powerfeedback.io/?${qs.stringify(query)}`);
  script.parentNode.insertBefore(iframe, script);
}

window.activateWidget = activateWidget;
