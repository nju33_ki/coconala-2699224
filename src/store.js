import {createStore} from 'redux';

const reducer = (state = {}, action) => {
  switch (action.type) {
    case 'init': {
      const {widget, referer} = action.payload;

      return {
        ...state,
        widget,
        referer,
      };
    }
    case 'patchWidget': {
      return {
        ...state,
        widget: {...state.widget, ...action.payload}
      };
    }
    default: {
      return state;
    }
  }
};

export default createStore(reducer);
