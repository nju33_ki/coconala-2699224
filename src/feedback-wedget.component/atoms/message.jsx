import styled from 'styled-components';

export const Message = styled.span`
  font-size: 1em;
  margin-bottom: 1em;
  font-weight: 500;
`;
