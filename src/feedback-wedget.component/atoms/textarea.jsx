import styled from 'styled-components';

export const Textarea = styled.textarea`
  box-sizing: border-box;
  width: 100%;
  padding: .375rem .75rem;
  margin-bottom: .5em;
  font-size: 1em;
  line-height: 1.5;
  color: #495057;
  border: 1px solid #ced4da;
  border-radius: .25rem;
  transition: .15s ease-in-out;
  outline: none;

  &:focus {
    border-color: #80bdff;
    box-shadow: 0 0 0px 2px #c5deff
  }
`;
