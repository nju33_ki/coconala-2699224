import camelcaseKeys from 'camelcase-keys';

export const init = ({widget, referer}) => {
  return {
    type: 'init',
    payload: {
      widget: camelcaseKeys(widget),
      referer,
    }
  }
}
