const path = require('path');

module.exports = {
  mode: process.env.NODE_ENV,
  entry: [path.join(__dirname, 'src/widget.js')],
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'widget.js'
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      }
    ]
  }
};
